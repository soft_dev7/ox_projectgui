/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ox_projectgui;

import java.util.Scanner;

/**
 *
 * @author KHANOMMECOM
 */
public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player O;
    private Player X;
    private int count;
    private boolean win = false;
    private boolean draw = false;

    public Board(Player O, Player X) {
        this.O = O;
        this.X = X;
        this.currentPlayer = O;
        this.count = 0;

    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return O;
    }

    public Player getX() {
        return X;
    }

    public int getCount() {
        return count;
    }

    private void switchPlayer() {
        if(currentPlayer==O){
            currentPlayer = X;
        }else {
            currentPlayer = O;
        }
    }
    public boolean isWin(){
        return win;
    }
    public boolean isDraw(){
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        if(isWin()||isDraw()) return false;
        if (col < 1 || row < 1 || col > 3 || row > 3) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if(checkWin(row, col)){
            if(this.currentPlayer==O){
               O.win();
               X.draw();
            }
            this.win=true;
            return true;
        }
        if(checkDraw()){
            this.draw=true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }
    private boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }
    private boolean checkWin(int row, int col) {
        if (checkVertical(row,col)) {
            return true;
        } else if (checkHorizontal(row,col)) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    public boolean checkVertical(int row, int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal(int row, int col) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) { // Arguments
            return true;
        }
        return false;
    }

    private boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;

    }

}
